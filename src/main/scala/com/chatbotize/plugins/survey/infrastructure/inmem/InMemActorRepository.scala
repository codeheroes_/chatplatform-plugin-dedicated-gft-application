package com.chatbotize.plugins.survey.infrastructure.inmem

import akka.Done
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.chatbotize.plugin.request.Request
import com.chatbotize.plugins.survey.domain.Defs.{ChatbotId, PlatformId, UserId}
import com.chatbotize.plugins.survey.domain.actor.{ActorRepository, SurveyActor}
import com.chatbotize.plugins.survey.infrastructure.kafka.KafkaResponseService

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

class InMemActorRepository(responseService: KafkaResponseService)(
    implicit system: ActorSystem,
    mat: ActorMaterializer,
    ec: ExecutionContext)
    extends ActorRepository {

  private implicit val timeout: Timeout = Timeout(10 seconds)
  private val proxyActor: ActorRef      = system.actorOf(Props(new ProxyActor))

  class ProxyActor extends Actor {
    private def getActor(platformId: PlatformId, chatbotId: ChatbotId, userId: UserId) = {
      val id = s"$platformId-$chatbotId-$userId"
      context
        .child(id)
        .getOrElse(context.actorOf(Props(new SurveyActor(platformId, chatbotId, userId)(responseService)), id))
    }

    override def receive: Receive = {
      case Request(_, _, platformId, chatbotId, userId, _, action) =>
        getActor(platformId, chatbotId, userId) forward action

    }
  }

  override def send(request: Request): Future[Done] =
    (proxyActor ? request).mapTo[Done]

}

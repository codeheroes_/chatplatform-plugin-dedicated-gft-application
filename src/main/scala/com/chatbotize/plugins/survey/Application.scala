package com.chatbotize.plugins.survey

import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.chatbotize.plugins.survey.Application.ServicesConfig
import com.chatbotize.plugins.survey.domain.Defs
import com.chatbotize.plugins.survey.domain.processor.MessageProcessor
import com.chatbotize.plugins.survey.infrastructure.inmem.InMemActorRepository
import com.chatbotize.plugins.survey.infrastructure.kafka._
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContextExecutor

object Application {
  final case class ServicesConfig(kafka: KafkaConfig)
  final case class KafkaConfig(host: String, port: Int) {
    def stringify: String = s"$host:$port"
  }

}

class Application(config: Config, services: ServicesConfig) extends StrictLogging {

  private implicit val system: ActorSystem          = ActorSystem("ApplicationActorSystem", config)
  private implicit val mat: ActorMaterializer       = ActorMaterializer()
  private implicit val ec: ExecutionContextExecutor = system.dispatcher

  private val requestSource = new KafkaRequestsSource(
    groupId = s"plugin-${Defs.PLUGIN_ID}",
    clientId = UUID.randomUUID().toString,
    bootstrapServers = services.kafka.stringify,
    topic = s"plugin-${Defs.PLUGIN_ID}-requests"
  )
  private val responseSink = new KafkaResponseService(
    topic = "plugin-responses",
    bootstrapServers = services.kafka.stringify
  )

  private val actorRepository =
    new InMemActorRepository(responseSink)

  private val messageProcessor =
    new MessageProcessor(requestSource.source(), actorRepository)

  def start(): Unit =
    messageProcessor.initialize()
}

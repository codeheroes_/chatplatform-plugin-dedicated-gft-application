package com.chatbotize.plugins.survey.domain

object Defs {

  type InstanceId = String
  type UserId     = String
  type PlatformId = String
  type ChatbotId  = String

  val PLUGIN_ID   = "dedicated-gft-application"
  val INSTANCE_ID = "default"

  val BUTTON_YES = "BUTTON_YES"
  val BUTTON_NO  = "BUTTON_NO"
}

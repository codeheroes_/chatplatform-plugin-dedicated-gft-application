package com.chatbotize.plugins.survey.domain.actor

import akka.Done
import akka.actor.{Actor, Stash, Status, Timers}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import com.chatbotize.plugin.request.Request
import com.chatbotize.plugin.request.Request.Action.{Abort, Empty, Initialize, Message}
import com.chatbotize.plugin.response._
import com.chatbotize.plugins.survey.domain.Defs
import com.chatbotize.plugins.survey.domain.Defs.{ChatbotId, PlatformId, UserId}
import com.chatbotize.plugins.survey.domain.actor.SurveyActor.{FieldId, Fields}
import com.chatbotize.plugins.survey.infrastructure.kafka.KafkaResponseService
import com.chatbotize.protocol.request.Message.Payload
import com.chatbotize.protocol.request.{Message => RequestMessage}
import com.chatbotize.protocol.response
import com.chatbotize.protocol.response.Message.{Payload => RPayload}
import com.chatbotize.protocol.response.{MessageTag, Message => ResponseMessage}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Random, Success}

class SurveyActor(platformId: PlatformId, chatbotId: ChatbotId, userId: UserId)(
    responseService: KafkaResponseService
)(implicit ec: ExecutionContext, mat: ActorMaterializer)
    extends Actor
    with StrictLogging
    with Stash
    with Timers {

  override def receive: Receive = handling

  private def handling: Receive = {
    case action: Request.Action =>
      sender() ! Done
      action match {
        case Initialize(_)  => context.become(collecting(List.empty))
        case Message(value) => unexpected(value.message)
        case Empty          => finish()
        case Abort(_)       => abort()
      }

    case other => handleOther(other, "handling")

  }

  private def collecting(collected: List[FieldId]): Receive = {
    Random.shuffle(Fields.all.filter(f => !collected.contains(f))).headOption match {
      case Some(field) =>
        respondWith(List(RPayload.Text(response.Text(field.randomQuestion))))

      case None =>
        respondWith(
          List(RPayload.Text(response.Text("We have collected below data for your loan:"))) ++ collected.map(f =>
            RPayload.Text(response.Text(s"${f.name}: ${f.label}"))) :+ RPayload.QuickButtons(
            response.QuickButtons(
              "Is data correct?",
              List(
                response.QuickButton("Yes", Defs.BUTTON_YES),
                response.QuickButton("No", Defs.BUTTON_NO)
              ))),
        )
    }

    {
      case action: Request.Action =>
        sender() ! Done
        action match {
          case Initialize(_) => context.become(collecting(List.empty))

          case Message(value) =>
            value.message.payload match {

              case Payload.Text(txt) =>
                val newFields = Fields.all.filter(f => txt.value.contains(f.label))
                context.become(collecting(collected ++ newFields))

              case Payload.Button(btn) =>
                btn.buttonId match {
                  case Defs.BUTTON_YES =>
                    respondWithAndFinish(List(RPayload.Text(response.Text(
                      "Thank you! Your application has been genrated and send to our loan department :){{finishApplication}}"))))

                  case Defs.BUTTON_NO =>
                    respondWithAndFinish(
                      List(RPayload.Text(response.Text("Data change will be available in the future :)"))))

                  case _ => context.become(collecting(collected))

                }

              case _ => context.become(collecting(collected))
            }
          case Empty    => finish()
          case Abort(_) => abort()
        }

      case other => handleOther(other, "handling")

    }
  }

  private def logError(message: String, ex: Throwable): Unit =
    logger.error(s"$message for platformId: $platformId, chatbotId: $chatbotId, userId: $userId", ex)

  private def logWarn(message: String): Unit =
    logger.warn(s"$message for platformId: $platformId, chatbotId: $chatbotId, userId: $userId")

  private def respond(actions: Response.Action*): Unit =
    Source(actions.toList)
      .mapAsync(1)(
        action =>
          responseService
            .respond(
              Response(
                pluginId = Defs.PLUGIN_ID,
                instanceId = Defs.INSTANCE_ID,
                platformId = platformId,
                chatbotId = chatbotId,
                userId = userId,
                action = action
              ))
      )
      .runWith(Sink.ignore)
      .onComplete {
        case Success(_)  => //ignore
        case Failure(ex) => logError("Error while sending response", ex)
      }

  private def finish(): Unit = {
    respond(Response.Action.LoudFinish(ActionLoudFinish()))
    context.stop(self)
  }

  private def unexpected(message: RequestMessage): Unit =
    respond(Response.Action.Unexpected(ActionUnexpected(message)))

  private def respondWith(payloads: Seq[RPayload]): Unit =
    respond(Response.Action.Response(ActionResponse(payloads.map(p => ResponseMessage(MessageTag.DEFAULT, p)))))

  private def respondWithAndFinish(payload: Seq[RPayload]): Unit = {
    respond(
      Response.Action.Response(ActionResponse(payload.map(p => ResponseMessage(MessageTag.DEFAULT, p)))),
      Response.Action.LoudFinish(ActionLoudFinish()))

  }

  private def handleOther(other: Any, state: String): Unit = {
    logWarn(s"Unexpected $other command in $state state")
    if (sender() != self) {
      sender() ! Status.Failure(new IllegalStateException(s"Unexpected $other command in initialized state"))
    }
  }

  private def abort(): Unit = context.stop(self)

}

object SurveyActor {
  sealed trait FieldId {
    def label: String

    def name: String

    def questions: List[String]

    def randomQuestion: String = s"${Random.shuffle(questions).head}$label"
  }

  object Fields {
    final case object Name extends FieldId {
      override def label: String = "{{name}}"

      override def questions: List[String] =
        List("What is your name?", "Please write your name:", "Can you tell my your name, please?")

      override def name: String = "Name"
    }
    final case object Address extends FieldId {
      override def label: String = "{{address}}"

      override def questions: List[String] =
        List("Where do you leave?", "What is your address?", "Please, tell me your address:")

      override def name: String = "Address"
    }
    final case object City extends FieldId {
      override def label: String = "{{city}}"

      override def questions: List[String] = List("In what city do you leave?", "Ok, so in which city do you leave?")

      override def name: String = "City"
    }
    final case object BirthDay extends FieldId {
      override def label: String = "{{birthDay}}"

      override def questions: List[String] = List("When where you born?", "What is your date of birth?")

      override def name: String = "Birth date:"
    }
    final case object Pesel extends FieldId {
      override def label: String = "{{pesel}}"

      override def questions: List[String] =
        List("What is your insurance number?", "Please, tell me your insurance number:")

      override def name: String = "Insurance number"
    }
    final case object PersonalId extends FieldId {
      override def label: String = "{{personalId}}"

      override def questions: List[String] = List("Now it`s time to tell me personal id:", "What is your personal id?")

      override def name: String = "Personal ID"
    }
    final case object MoneyAmount extends FieldId {
      override def label: String = "{{moneyAmount}}"

      override def questions: List[String] = List("What is the amount of loan?", "How much money do you need?")

      override def name: String = "Amount"
    }
    final case object Period extends FieldId {
      override def label: String = "{{period}}"

      override def questions: List[String] =
        List("For how long you need the loan?", "In what period would you like to repay?")

      override def name: String = "Period"
    }

    val all = List(Name, Address, City, BirthDay, Period, PersonalId, MoneyAmount, Period)
  }

}

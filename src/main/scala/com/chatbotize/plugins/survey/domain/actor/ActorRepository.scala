package com.chatbotize.plugins.survey.domain.actor

import akka.Done
import com.chatbotize.plugin.request.Request

import scala.concurrent.Future

trait ActorRepository {

  def send(request: Request): Future[Done]

}

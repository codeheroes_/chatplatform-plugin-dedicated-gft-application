package com.chatbotize.plugins.survey.domain.processor

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import com.chatbotize.plugin.request.Request
import com.chatbotize.plugins.survey.domain.actor.ActorRepository
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

class MessageProcessor(source: Source[Request, NotUsed], actorRepository: ActorRepository)(
    implicit system: ActorSystem,
    mat: ActorMaterializer,
    ec: ExecutionContext)
    extends StrictLogging {

  private def delayedRestart() =
    system.scheduler.scheduleOnce(5 seconds, () => start())

  private def start(): Unit = {
    source
      .mapAsyncUnordered(32)(request => {
        actorRepository
          .send(request)
          .recover {
            case ex: Throwable =>
              logger.error(s"Error while processing request $request", ex)
              List.empty
          }
      })
      .runWith(Sink.ignore)
      .onComplete {
        case Success(_) =>
          logger.warn(s"MessageProcessor stopped. Restarting...")
          delayedRestart()

        case Failure(ex) =>
          logger.error(s"MessageProcessor failed. Restarting...", ex)
          delayedRestart()
      }
  }

  def initialize(): Unit = start()
}
